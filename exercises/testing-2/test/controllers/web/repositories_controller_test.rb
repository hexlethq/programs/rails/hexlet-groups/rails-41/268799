# frozen_string_literal: true

require 'test_helper'
require 'octokit'

module Web
  class RepositoriesControllerTest < ActionDispatch::IntegrationTest
    # BEGIN
    setup do
      @repo = repositories :one

      @attrs = {
        link: 'https://github.com/octocat/Hello-World'
      }
    end

    test 'get index' do
      get repositories_url
      assert_response :success
    end

    test 'get new' do
      get new_repository_url
      assert_response :success
    end

    test 'should create' do
      uri_template = Addressable::Template.new 'https://api.github.com/repos/{owner_name}/{repo_name}'

      response = load_fixture('files/response.json')

      stub_request(:get, uri_template)
        .to_return(
          status: 200,
          body: response,
          headers: { 'Content-Type' => 'application/json' }
        )

      post repositories_url, params: {
        repository: @attrs
      }

      assert_response :redirect

      repository = Repository.find_by! link: @attrs[:link]
      assert { repository.description.present? }

      response_json = JSON.parse response

      assert { @attrs[:link] == repository.link }
      assert_equal response_json['owner']['login'], repository.owner_name

      assert_redirected_to repository_url(repository)
    end

    test 'get edit' do
      get edit_repository_url(@repo)
      assert_response :success
    end

    test 'should update' do
      patch repository_url(@repo), params: {
        repository: @attrs
      }
      assert_response :redirect

      @repo.reload

      assert { @repo.link == @attrs[:link] }
    end

    test 'destroy' do
      delete repository_url(@repo)

      assert_response :redirect
      assert { !Repository.exists? @repo.id }
    end
    # END
  end
end
