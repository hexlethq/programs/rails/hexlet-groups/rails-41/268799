# frozen_string_literal: true

require_relative '../test_helper'

class TaskTest < ActiveSupport::TestCase
  def setup
    @task = Task.create(
      name: 'Task 1',
      description: 'Task 1 desc',
      creator: 'Anton'
    )
    @task_id = @task.id
  end

  test 'create task' do
    assert_equal 'Task 1', @task.name
    assert_equal 'Task 1 desc', @task.description
    assert_equal 'backlog', @task.status
    assert_equal 'Anton', @task.creator
    assert_nil @task.performer
    assert_equal false, @task.completed
  end

  test 'update task' do
    options = {
      name: 'Task 1 updated',
      description: 'Task 1 desc updated'
    }

    assert @task.update(options)

    @task = Task.find(@task_id)

    assert_equal 'Task 1 updated', @task.name
    assert_equal 'Task 1 desc updated', @task.description
  end

  test 'check validation task' do
    options = {
      name: ''
    }
    assert_not @task.update(options)
  end

  test 'destroy task' do
    assert @task.destroy
    assert_nil Task.find_by(id: @task_id)
  end
end
