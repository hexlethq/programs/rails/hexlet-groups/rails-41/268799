# frozen_string_literal: true

# BEGIN

require 'uri'
require 'forwardable'

# Url class
class Url
  include Comparable

  attr_reader :uri

  extend Forwardable

  def initialize(url)
    @uri = URI(url)
  end

  def_delegators :@uri, :scheme, :host

  def query_params
    uri.query.split('&').reduce({}) do |memo, pair|
      key, value = pair.split('=')
      memo[key.to_sym] = value
      memo
    end
  end

  def query_param(key, default = nil)
    query_params[key] || default
  end

  def <=>(other)
    uri <=> other.uri
  end
end
# END
