# frozen_string_literal: true

module Web
  module Movies
    # class ReviewsController
    class ReviewsController < Web::Movies::ApplicationController
      def index
        @reviews = @resource_movie.reviews
      end

      def new
        @review = Review.new
      end

      def create
        @review = Review.new(resource_params)
        @review.movie = @resource_movie

        if @review.save
          redirect_to movie_reviews_path(@resource_movie), notice: t('success')
        else
          redirect_to :new, notice: t('fail')
        end
      end

      def edit
        @review = resource
      end

      def update
        if resource.update(resource_params)
          redirect_to movie_reviews_path(@resource_movie), notice: t('success')
        else
          redirect_to :edit, notice: t('fail')
        end
      end

      def destroy
        if resource.destroy
          redirect_to movie_reviews_path(@resource_movie), notice: t('.success')
        else
          redirect_to movie_reviews_path(@resource_movie), notice: t('.fail')
        end
      end

      protected

      def resource
        Review.find params[:id]
      end

      def resource_params
        params.require(:review).permit(:body)
      end
    end
  end
end
