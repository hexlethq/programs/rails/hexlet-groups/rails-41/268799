# frozen_string_literal: true

module Web
  module Movies
    # class ApplicationController
    class ApplicationController < Web::ApplicationController
      helper_method :resource_movie

      before_action :resource_movie

      def resource_movie
        # BEGIN
        @resource_movie ||= Movie.find(params[:movie_id])
        # END
      end
    end
  end
end
