# frozen_string_literal: true

module Web
  module Movies
    # class CommentsController
    class CommentsController < Web::Movies::ApplicationController
      def index
        @comments = @resource_movie.comments
      end

      def new
        @comment = Comment.new
      end

      def create
        @comment = Comment.new(resource_params)
        @comment.movie = @resource_movie

        if @comment.save
          redirect_to movie_comments_path(@resource_movie), notice: t('success')
        else
          redirect_to :new, notice: t('fail')
        end
      end

      def edit
        @comment = resource
      end

      def update
        if resource.update(resource_params)
          redirect_to movie_comments_path(@resource_movie), notice: t('success')
        else
          redirect_to :edit, notice: t('fail')
        end
      end

      def destroy
        if resource.destroy
          redirect_to movie_comments_path(@resource_movie), notice: t('.success')
        else
          redirect_to movie_comments_path(@resource_movie), notice: t('.fail')
        end
      end

      protected

      def resource
        Comment.find params[:id]
      end

      def resource_params
        params.require(:comment).permit(:body)
      end
    end
  end
end
