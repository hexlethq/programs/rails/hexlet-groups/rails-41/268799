# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  attr_reader :stack

  def setup
    @stack = Stack.new
  end

  def test_empty
    assert_equal stack.size, 0
    assert(stack.empty?)
  end

  def test_push
    stack.push! 'ruby'
    assert_equal stack.size, 1

    stack.push! 'js'
    stack.push! 'php'
    stack.push! 'java'

    assert_equal stack.size, 4
  end

  def test_to_a
    stack.push! 'js'
    stack.push! 'php'
    stack.push! 'java'

    assert_equal stack.to_a, %w[js php java]
  end

  def test_pop
    stack.push! 'php'
    stack.push! 'java'
    assert_equal stack.pop!, 'java'
    assert_equal stack.size, 1
    assert_equal stack.pop!, 'php'
    assert_nil(stack.pop!)
  end

  def test_size
    assert_equal stack.size, 0

    stack.push! 'js'
    stack.push! 'php'

    assert_equal stack.size, 2
  end

  def test_clear
    stack.push! 'js'
    stack.push! 'php'

    assert_equal stack.size, 2

    stack.clear!

    assert(stack.empty?)
    assert_equal stack.to_a, []
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.to_s.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
