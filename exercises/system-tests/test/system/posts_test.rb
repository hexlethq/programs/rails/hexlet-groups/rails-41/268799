# frozen_string_literal: true

require_relative '../application_system_test_case'

# BEGIN
class PostsTest < ApplicationSystemTestCase
  setup do
    @post = posts(:one)
  end

  test 'visiting to Posts' do
    visit posts_url

    assert_selector 'h1', text: 'Posts'
  end

  test 'visiting to New Post' do
    visit posts_url
    click_on 'New Post'

    assert_selector 'h1', text: 'New post'
  end

  test 'creating a Post' do
    visit posts_url
    click_on 'New Post'

    fill_in 'Title', with: 'New post title'
    fill_in 'Body', with: 'New post body'
    click_button 'Create Post'

    assert_text 'Post was successfully created.'
    click_on 'Back'
  end

  test 'visiting to Post' do
    visit posts_url
    click_on 'Show', match: :first

    assert_selector 'h1', text: 'Two'
    click_on 'Back'
  end

  test 'creating a Post Comment' do
    visit posts_url
    click_on 'Show', match: :first

    fill_in with: 'New post comment body'
    click_button 'Create Comment'

    assert_text 'New post comment body'
  end

  test 'updating a Post' do
    visit posts_url
    click_on 'Edit', match: :first

    fill_in 'Title', with: @post.title
    click_button 'Update Post'

    assert_text 'Post was successfully updated.'
    click_on 'Back'
  end

  test 'destroying a Post' do
    visit posts_url

    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Post was successfully destroyed.'
  end
end
# END
