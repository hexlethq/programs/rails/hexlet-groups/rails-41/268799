require 'csv'

namespace :hexlet do
  desc 'Import users from CSV'

  task :import_users, %i[data_path] => :environment do |_task, args|
    fields, *users = CSV.read(args[:data_path])

    users.each do |user|
      params = Hash[fields.zip(user)]
      User.create(params)
    end
  end
end
