# frozen_string_literal: true

# BEGIN
def compare_versions(ver1, ver2)
  version1 = ver1.split('.').map(&:to_i)
  version2 = ver2.split('.').map(&:to_i)

  version1 <=> version2
end
# END
