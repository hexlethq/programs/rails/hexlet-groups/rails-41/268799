require_relative '../test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  self.use_transactional_tests = true

  test 'open all bulletins page' do
    get bulletins_path
    assert_response :success
    assert_select 'h1', 'Bulletins'
    assert_select 'li.bulletin-item', 3
  end

  test 'open one bulletin page' do
    get bulletin_path(bulletins(:bulletin_1))

    assert_response :success
    assert_select 'h1', 'Bulletin one'
    assert_select 'p', 'bulletin one test body'
  end
end