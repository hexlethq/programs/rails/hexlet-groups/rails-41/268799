# frozen_string_literal: true

# BEGIN
def get_same_parity(list)
  head = list.first
  is_same_parity = ->(num) { head.even? == num.even? }
  list.select(&is_same_parity)
end
# END
