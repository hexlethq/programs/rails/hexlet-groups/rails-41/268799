# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, list)
  sort_by_letters = ->(str) { str.split('').sort.join }
  sort_by_letters_word = sort_by_letters.call(word)
  is_anagram = ->(str) { sort_by_letters.call(str).eql? sort_by_letters_word }
  list.select(&is_anagram)
end
# END
