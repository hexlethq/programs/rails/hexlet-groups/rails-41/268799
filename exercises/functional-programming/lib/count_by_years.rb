# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  is_male = ->(user) { user[:gender] == 'male' }
  get_birthday = ->(user) { user[:birthday].slice(0..3) }

  users.select(&is_male).reduce({}) do |memo, user|
    year = get_birthday.call(user)

    memo[year] = memo.key?(year) ? memo[year] + 1 : 1
    memo
  end
end
# END
