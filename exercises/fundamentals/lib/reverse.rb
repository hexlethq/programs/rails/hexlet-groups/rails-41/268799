# frozen_string_literal: true

# BEGIN
def reverse(string)
  chars = string.split('')
  chars.map.with_index { |c, i| chars[chars.size - 1 - i] }.join
end
# END
