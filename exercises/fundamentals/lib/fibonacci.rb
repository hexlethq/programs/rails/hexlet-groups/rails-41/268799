# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  return nil if num <= 0
  return 0 if num == 1

  iter = ->(acc = 2, a = 0, b = 1) {
    acc == num ? b : iter.call(acc + 1, b, a + b)
  }

  iter.call()
end
# END
