# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  (start..stop).map do |num|
    if (num % 15).zero?
      'FizzBuzz'
    elsif (num % 3).zero?
      'Fizz'
    elsif (num % 5).zero?
      'Buzz'
    else num
    end
  end.join(' ')
end
# END
