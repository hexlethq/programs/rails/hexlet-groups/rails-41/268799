# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  attr_reader :app

  def initialize(app)
    @app = app
  end

  def call(env)
    locale_from_header = env['HTTP_ACCEPT_LANGUAGE']&.scan(/^[a-z]{2}/)&.first

    I18n.locale = locale_from_header || I18n.default_locale
    Rails.logger.debug "Header locale: #{locale_from_header}"
    Rails.logger.debug "Set locale: #{I18n.locale}"

    @app.call(env)
  end
  # END
end
