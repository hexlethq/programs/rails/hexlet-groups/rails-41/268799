require_relative '../test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task = tasks(:one)

    @attrs = {
      name: Faker::Lorem.sentence(word_count: 3),
      description: Faker::Lorem.paragraph,
      creator: Faker::Name.name
    }
  end

  test 'should get index' do
    get tasks_url
    assert_response :success
    assert_select 'h1', 'Tasks'
  end

  test 'should get new' do
    get new_task_url
    assert_response :success
  end

  test 'create task' do
    post tasks_path params: { task: @attrs }

    task = Task.find_by! name: @attrs[:name]

    assert_equal @attrs[:name], task.name
    assert_equal @attrs[:description], task.description
    assert_equal Task::DEFAULT_STATUS, task.status
    assert_equal @attrs[:creator], task.creator
    assert_nil task.performer
    assert_equal false, task.completed
  end

  test 'open show task' do
    get task_path(@task)

    assert_response :success
  end

  test 'should get edit' do
    get edit_task_url(@task)
    assert_response :success
  end

  test 'update task' do
    patch task_url(@task), params: { task: @attrs }
    assert_redirected_to task_url(@task)

    @task.reload

    assert { @task.name == @attrs[:name] }
    assert { @task.description == @attrs[:description] }
    assert { @task.creator == @attrs[:creator] }
  end

  test 'should destroy task' do
    delete task_url(@task)

    assert { !Task.exists? @task.id }

    assert_redirected_to tasks_url
  end
end
