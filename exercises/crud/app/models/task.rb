class Task < ApplicationRecord
  validates :name, :status, :creator, presence: true

  DEFAULT_STATUS = 'backlog'.freeze
end
