# frozen_string_literal: true

class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.string :name
      t.text :description

      t.timestamps

      t.references :status, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
    end
  end
end
