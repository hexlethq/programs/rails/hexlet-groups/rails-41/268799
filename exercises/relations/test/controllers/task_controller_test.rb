# frozen_string_literal: true

require_relative '../test_helper'

class TaskControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task = tasks(:two)

    @attrs = {
      name: Faker::Movies::BackToTheFuture.character,
      description: Faker::Lorem.paragraph,
      user_id: users(:one).id
    }
  end

  test 'should get index' do
    get tasks_url
    assert_response :success
  end

  test 'should get new' do
    get new_task_url
    assert_response :success
  end

  test 'should create task' do
    post tasks_url, params: { task: @attrs }

    task = Task.find_by! name: @attrs[:name]

    assert_redirected_to task_url(task)
  end

  test 'should show task' do
    get task_url(@task)
    assert_response :success
  end

  test 'should get edit' do
    get edit_task_url(@task)
    assert_response :success
  end

  test 'should update task' do
    patch task_url(@task), params: { task: @attrs }
    assert_redirected_to task_url(@task)

    @task.reload

    assert { @task.name == @attrs[:name] }
  end

  test 'should destroy task' do
    delete task_url(@task)

    assert { !Task.exists? @task.id }

    assert_redirected_to tasks_url
  end
end
