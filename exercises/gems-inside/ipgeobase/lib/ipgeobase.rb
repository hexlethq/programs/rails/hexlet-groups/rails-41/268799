# frozen_string_literal: true

require "addressable/uri"

require_relative "ipgeobase/version"
require_relative "ipgeobase/ip_meta_data"

# Ipgeobase module
module Ipgeobase
  def self.lookup(ip)
    uri = Addressable::URI.parse("http://ip-api.com/xml/#{ip}")
    req = Net::HTTP.get_response(uri)

    IpMetaData.parse(req.body)
  end
end
