# frozen_string_literal: true

class PostsController < ApplicationController
  after_action :verify_authorized, except: %i[index show]

  # BEGIN
  def index
    @posts = Post.all
  end

  def show
    @post = resource
  end

  def new
    @post = Post.new
    authorize @post
  end

  def create
    @post = Post.new(post_params)
    @post.author = current_user
    authorize @post

    if @post.save
      redirect_to @post, notice: 'Post was successfully created.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @post = resource
    authorize @post
  end

  def update
    @post = resource
    authorize @post

    if @post.update(post_params)
      redirect_to @post, notice: 'Post was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @post = resource
    authorize @post

    @post.destroy

    redirect_to posts_url, notice: 'Post was successfully destroyed.'
  end

  private

  def resource
    Post.find params[:id]
  end

  def post_params
    params.require(:post).permit(:title, :body)
  end
  # END
end
