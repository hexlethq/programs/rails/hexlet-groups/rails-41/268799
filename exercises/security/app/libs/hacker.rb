# frozen_string_literal: true

require 'open-uri'

class Hacker
  class << self
    def hack(email, password)
      # BEGIN
      hostname = 'https://rails-collective-blog-ru.hexlet.app'
      reg_page_path = 'users/sign_up'
      reg_action_path = 'users'

      response = Net::HTTP.get_response(URI.join(hostname, reg_page_path))
      response_html = Nokogiri::HTML(response.body)

      authenticity_token = response_html.at('html body input[name="authenticity_token"]')['value']
      cookies = response['Set-Cookie'].split('; ')[0]

      request_uri = URI.join(hostname, reg_action_path)

      params = {
        authenticity_token: authenticity_token,
        user: {
          email: email,
          password: password,
          password_confirmation: password
        }
      }

      body = URI.encode_www_form params

      Net::HTTP.post request_uri, body, cookie: cookies
    # END
    end
  end
end
