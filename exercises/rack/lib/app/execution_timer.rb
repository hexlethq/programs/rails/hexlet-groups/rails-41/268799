class ExecutionTimer
  def initialize(app)
    @app = app
  end

  def call(env)
    before = Time.now.to_f
    status, headers, body = @app.call(env)
    after = Time.now.to_f
    diff = (after - before) * 1_000_000
    log_message = "App took #{diff.to_i} milliseconds."

    [status, headers, [body, log_message].join("\n")]
  end
end