require 'digest'

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body = @app.call(env)
    log_message = Digest::SHA256.hexdigest body

    [status, headers, [body, log_message].join("\n")]
  end
end