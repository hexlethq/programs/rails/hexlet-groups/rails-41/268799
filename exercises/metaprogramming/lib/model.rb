# frozen_string_literal: true

# BEGIN
require 'date'

# Model module
module Model
  def initialize(attributes = {})
    @attributes = {}
    self.class.attribute_options.each do |name, options|
      value = attributes.key?(name) ? attributes[name] : options.fetch(:default, nil)
      write_attribute(name, value)
    end
  end

  def write_attribute(name, value)
    options = self.class.attribute_options[name]
    @attributes[name] = self.class.convert(value, options[:type])
  end

  # ClassMethods module
  module ClassMethods
    def attribute_options
      @attribute_options || {}
    end

    def attribute(name, options = {})
      @attribute_options ||= {}
      @attribute_options[name] = options

      define_method :"#{name}" do
        @attributes[name]
      end
      define_method :"#{name}=" do |value|
        write_attribute(name, value)
      end
    end

    def convert(value, type)
      return value if value.nil?

      case type
      when :integer then Integer value
      when :string then String value
      when :datetime then DateTime.parse value
      when :boolean then !!value
      else value
      end
    end
  end

  def self.included(base)
    base.attr_reader :attributes
    base.extend ClassMethods
  end
end
# END
