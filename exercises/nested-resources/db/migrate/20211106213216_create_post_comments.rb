# frozen_string_literal: true

# CreatePostComments Class
class CreatePostComments < ActiveRecord::Migration[6.1]
  def change
    create_table :post_comments do |t|
      t.text :content
      t.references :post, index: true, foreign_key: true

      t.timestamps
    end
  end
end
