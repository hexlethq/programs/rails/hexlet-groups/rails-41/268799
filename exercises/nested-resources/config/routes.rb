# frozen_string_literal: true

Rails.application.routes.draw do
  root 'homes#index'

  # BEGIN
  resources 'posts' do
    member do
      scope module: :posts do
        resources 'comments', only: %w[create destroy update edit], shallow: true
      end
    end
  end
  # END
end
