# frozen_string_literal: true

module Posts
  # CommentsController Class
  class CommentsController < ApplicationController
    def create
      @post_comment = PostComment.new(post_comment_params.merge(post_id: params[:id]))
      if @post_comment.save
        redirect_to resource_post, notice: 'Comment was successfully created.'
      else
        flash[:alert] = 'Comment has not been created.'
        render :edit, status: :unprocessable_entity
      end
    end

    def edit
      @comment = PostComment.find(params[:id])
    end

    def update
      @comment = PostComment.find params[:id]

      if @comment.update(post_comment_params)
        redirect_to post_path(@comment.post), notice: 'Comment was successfully updated.'
      else
        flash[:alert] = 'Comment has not been updated.'
        render :edit, status: :unprocessable_entity
      end

    end

    def destroy
      @post_comment = PostComment.find(params[:id])
      @post_comment.destroy

      redirect_back(fallback_location: root_path, notice: 'Comment was successfully destroyed.')
    end

    private

    def post_comment_params
      params.require(:post_comment).permit(:content)
    end
  end
end
