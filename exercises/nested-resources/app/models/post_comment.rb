# frozen_string_literal: true

class PostComment < ApplicationRecord
  belongs_to :post

  validates :content, presence: true, length: { minimum: 1, maximum: 500 }
end
